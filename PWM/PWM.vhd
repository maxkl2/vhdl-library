--------------------------------------------------------------------------------
-- Copyright (c) 2020 Max Klein <max@maxkl.de>
-- 
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.
--
-- A simple PWM module that's generic over the counter width and period
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PWM is
    generic (
        WIDTH : natural;  -- Width of the counter and compare registers in bits
        PERIOD : natural range 1 to natural'high  -- Period; make sure that PERIOD < 2**WIDTH
    );

    port (
        clk_in : in std_logic;  -- Clock
        reset_in : in std_logic;  -- Active high asynchronous reset

        compare_in : in std_logic_vector(WIDTH - 1 downto 0);  -- Compare register; 0 corresponds to a duty cycle of 0%, PERIOD to 100%

        q_out : out std_logic  -- Output signal
    );
end entity PWM;

architecture rtl of PWM is
    constant PERIOD_MINUS_ONE : unsigned(WIDTH - 1 downto 0) := to_unsigned(PERIOD - 1, WIDTH);

    signal counter : unsigned(WIDTH - 1 downto 0);
    signal q : std_logic;
begin
    count : process (clk_in, reset_in)
    begin
        if reset_in = '1' then
            counter <= PERIOD_MINUS_ONE;
        elsif rising_edge(clk_in) then
            if counter = (counter'range => '0') then
                counter <= PERIOD_MINUS_ONE;
            else
                counter <= counter - 1;
            end if;
        end if;
    end process count;

    q <= '1' when counter < unsigned(compare_in) else '0';

    q_out <= q;
end architecture rtl;
