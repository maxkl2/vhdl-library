--------------------------------------------------------------------------------
-- Copyright (c) 2020 Max Klein <max@maxkl.de>
-- 
-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.
--
-- Testbench for PWM.vhd
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity PWM_tb is
--  Port ( );
end PWM_tb;

architecture test of PWM_tb is
    constant CLK_PERIOD : time := 8 ns;
    constant CLK_HALF_PERIOD : time := CLK_PERIOD / 2;

    -- Inputs
    signal clk     : std_logic := '0';
    signal reset   : std_logic := '1';
    signal compare : std_logic_vector(7 downto 0) := "00000000";

    -- Outputs
    signal q      : std_logic;

    -- Helper
    signal sim_end : std_logic := '0';
begin

    UUT: entity work.PWM
        generic map (WIDTH => 8, PERIOD => 10)
        port map (clk_in => clk, reset_in => reset, compare_in => compare, q_out => q);

    process
    begin
        clk <= not clk;
        wait for CLK_HALF_PERIOD;

        if sim_end = '1' then
            wait;
        end if;
    end process;

    -- Stimulus process
    stimulus : process
    begin
        -- Configure
        compare <= std_logic_vector(to_unsigned(2, compare'length));

        -- Hold reset
        wait for 100 ns;
        reset <= '0';

        wait for 500 ns;

        compare <= std_logic_vector(to_unsigned(0, compare'length));

        wait for 500 ns;

        compare <= std_logic_vector(to_unsigned(9, compare'length));

        wait for 500 ns;

        compare <= std_logic_vector(to_unsigned(10, compare'length));

        wait for 500 ns;

        compare <= std_logic_vector(to_unsigned(11, compare'length));

        wait for 500 ns;

        report "PWM testbench completed";
        sim_end <= '1';

        wait;
    end process;

end test;
